const https = require('https')
const express = require('express')
const fs = require('fs')
const passport = require('passport')
const routes = require('./routes')
const PORT = 4000
const sslOptions = {
  key: fs.readFileSync('./sslKeys/ca.key'),
  cert: fs.readFileSync('./sslKeys/ca.crt')
}

class LaTeamDesNulos {
  constructor() {
    this.app = express()
    this.passport = passport
    this.run()
  }

  middlewares() {
    this.app.use(express.static(__dirname + '/public/templates/'))
    this.app.use(passport.initialize())
    this.app.use(passport.session())
    this.passport.serializeUser(function(user, done) {
      done(null, user)
    })
    this.passport.deserializeUser(function(user, done) {
      done(null, user)
    })
  }

  routes() {
    new routes.FacebookAuth(this.app, this.passport)
    new routes.GithubAuth(this.app, this.passport)
    new routes.GoogleAuth(this.app, this.passport)
    this.app.get('/', function(req, res) {
      res.sendFile(__dirname + '/public/templates/index.html')
    })
    this.app.get('/profile', function(req, res) {
      res.send('<h1>Bienvenue '+ req.query.fullname +'</h1>')
    })
  }

  run() {
    this.middlewares()
    this.routes()
    https
      .createServer(sslOptions, this.app)
      .listen(PORT, function() {
        console.log(`listening on https://localhost:${PORT} with autosigned certs (HTTPS)`)
      })
  }
}

const SGS = new LaTeamDesNulos()
