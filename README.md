# La Team Des Nulos

Un projet dont le but est de tester la connexion par facebook, google et github.

## Procédure

```
> git clone https://gitlab.com/suleydogan70/cybersecurite.git
> cd cybersecurite
> npm install
> npm start
```

Et rendez vous sur https://localhost:4000

Pour Facebook, il y a des identifiants spécifiques (juste en dessous), pour le reste vous pouvez utiliser vos identifiants.

## Identifiants FB

nom de compte : dnumlztrll_1590595030@tfbnw.net

mot de passe :Test123test
