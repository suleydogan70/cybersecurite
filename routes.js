const FacebookAuth = require('./auth/facebook')
const GithubAuth = require('./auth/github')
const GoogleAuth = require('./auth/google')


module.exports = {
  FacebookAuth,
  GithubAuth,
  GoogleAuth
}
