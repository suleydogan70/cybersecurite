const url = require('url')
const GitHubStrategy = require('passport-github2').Strategy
const GitHubconfig = require('./config')

module.exports = class GithubAuth {
  constructor(app, passport) {
    this.app = app
    this.passport = passport

    this.run()
  }

  routes() {
    this.passport.use(new GitHubStrategy({
        clientID: GitHubconfig.clientID,
        clientSecret: GitHubconfig.clientSecret,
        callbackURL: GitHubconfig.callbackURL
      },
      function(accessToken, refreshToken, profile, done) {
        done(null, profile)
      }
    ));
    this.app.get('/auth/github', this.passport.authenticate('github'));
    this.app.get('/auth/github/callback', this.passport.authenticate('github'),
    function(req, res) {
      res.redirect(url.format({
        pathname: '/profile',
        query: {
          fullname: req.user.username
        }
      }))
    });
  }

  run() {
    this.routes()
  }
}
