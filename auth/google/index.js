const url = require('url')
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
const ggconfig = require('./config')

module.exports = class GoogleAuth {
  constructor(app, passport) {
    this.app = app
    this.passport = passport

    this.run()
  }

  routes() {
    this.passport.use(new GoogleStrategy({
        clientID: ggconfig.GOOGLE_CLIENT_ID,
        clientSecret: ggconfig.GOOGLE_APP_SECRET,
        callbackURL: ggconfig.GOOGLE_CALLBACK_URL
      },
      function(accessToken, refreshToken, profile, done) {
        done(null, profile)
      }
    ))
    this.app.get('/auth/google', this.passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }))
    this.app.get('/auth/google/callback', this.passport.authenticate('google'), function(req, res) {
      res.redirect(url.format({
        pathname: '/profile',
        query: {
          fullname: req.user.displayName
        }
      }))
    })
  }

  run() {
    this.routes()
  }
}
