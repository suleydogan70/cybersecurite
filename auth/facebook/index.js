const url = require('url')
const FacebookStrategy = require('passport-facebook').Strategy
const fbconfig = require('./config')

module.exports = class FacebookAuth {
  constructor(app, passport) {
    this.app = app
    this.passport = passport

    this.run()
  }

  routes() {
    this.passport.use(new FacebookStrategy({
        clientID: fbconfig.FACEBOOK_CLIENT_ID,
        clientSecret: fbconfig.FACEBOOK_APP_SECRET,
        callbackURL: fbconfig.FACEBOOK_CALLBACK_URL
      },
      function(accessToken, refreshToken, profile, done) {
        done(null, profile)
      }
    ))
    this.app.get('/auth/facebook', this.passport.authenticate('facebook'))
    this.app.get('/auth/facebook/callback', this.passport.authenticate('facebook'), function(req, res) {
      res.redirect(url.format({
        pathname: '/profile',
        query: {
          fullname: req.user.displayName
        }
      }))
    })
  }

  run() {
    this.routes()
  }
}
